package net.dulatello08.mcservermobile.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import net.dulatello08.mcservermobile.R
import net.dulatello08.mcservermobile.ServerCore

class CreateServerFragment : Fragment() {
    private lateinit var serverSpinner: Spinner
    private lateinit var createServer: Button
    private lateinit var description: TextView
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewInit(view)
        val spinnerArray = resources.getStringArray(R.array.server_spinner_array)
        val descriptionArray = resources.getStringArray(R.array.server_description_array)
        val adapter =
            ArrayAdapter(requireContext(), R.layout.spinner_label_item, spinnerArray)
        adapter.setDropDownViewResource(R.layout.spinner_drop_item)
        serverSpinner.adapter = adapter
        serverSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                description.text = descriptionArray[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
        createServer.setOnClickListener {
            activity?.findViewById<FrameLayout>(R.id.server_progress)?.visibility = View.VISIBLE
            val serverCore = ServerCore()
            serverCore.startServer(spinnerArray[serverSpinner.selectedItemPosition])
        }
    }

    fun viewInit(view: View) {
        serverSpinner = view.findViewById(R.id.server_spinner)
        description = view.findViewById(R.id.server_description)
        createServer = view.findViewById(R.id.create_server)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }
}